import React from 'react'
import { Col, Container, Row, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from "react-redux";
import { LoginGoogle, LoginEmail } from '../config/redux/actions/authAction';
import { useNavigate } from "react-router-dom";
import {useState, useEffect} from 'react';
import '../style/login.css';


const Login = () => {
    const dispatch = useDispatch();
    
    const navigate = useNavigate();

    const { dataLogin } = useSelector((state) => state.auth);

    const [email, setEmail] = useState('')

    const [password, setPassword] = useState('')

    const handleLogin = async (e) => {
        e.preventDefault() 
        const data = await dispatch(LoginGoogle())
        if (data) navigate("/landingcar")
    }

    const handleLoginEmail = () => {
        dispatch(LoginEmail(email, password))
        setTimeout(() => {
          navigate("dashboard");
        }, 3000)
    }
    useEffect(() => {
      if (dataLogin?.email === "admin@admin.com") navigate("/dashboard");
      if (dataLogin?.email !== "admin@admin.com" && dataLogin !== null)
        navigate("/landingcar");
      // eslint-disable-next-line
    }, []);
  
    return (
    <>
        <section className="Form">
            <Container>
                <Row>
                    <Col md="6" className="img"></Col> 

                    <Col md="6">
                        <div className="logo-row"></div>
                            <h1 className="text">Welcome, Admin BCR</h1>
                        <form action="">
                            <div className="form-row">
                                <div className="col-lg-9">
                                    <h1 className="text1">Email</h1>
                                    <input type="email" placeholder="Contoh: johndee@gmail.com" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)} />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col-lg-9">
                                    <h1 className="text2">Password</h1>
                                    <input type="password" placeholder="6+ karakter" className="control" value={password} onChange={(e) => setPassword(e.target.value)} />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col-lg-9">
                                    <Button className="btn1" onClick={handleLoginEmail}>Sign In</Button>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col-lg-9">
                                    <button className="google" onClick={(e) => handleLogin(e)}>Login with Google</button>
                                </div>
                            </div>
                        </form>
                    </Col>
                </Row>
            </Container>
        </section>
    </>
  )
}

export default Login
import Register from './pages/Register'
import Login from './pages/Login'
import Dashboard from './pages/Home/Dashboard'
import LandingCar from './pages/Landingcar/LandingCar'
import {BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';


function App() {
  return (
    <Router>
      <div>
        <Routes>
          <Route path="/" element={<Register/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/dashboard" element= {<Dashboard/>} />
          <Route path="/landingcar" element={<LandingCar/>} />
        </Routes>
      </div>
    </Router> 
  );
}

export default App;
